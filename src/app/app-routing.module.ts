import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // { path: '', loadChildren: () => import('./Components/helloworld/helloworld.module').then(res => res.HelloworldModule) },
  { path: '', loadChildren: () => import('./Components/main/main.module').then(res => res.MainComponentModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
