import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormvalidationComponent } from '../../formvalidation/formvalidation.component';

@Component({
  selector: 'app-formdata',
  templateUrl: './formdata.component.html',
  styleUrls: ['./formdata.component.scss']
})
export class FormdataComponent implements OnInit {
  userdetails = []
  constructor(
    public dialogRef: MatDialogRef<FormvalidationComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
    this.userdetails.push(this.data)
    console.log(this.data)
  }
  Send() {
    console.log(this.data)
  }
}
