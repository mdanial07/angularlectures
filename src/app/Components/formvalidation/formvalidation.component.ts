import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FromService } from './formvalidation.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormdataComponent } from '../Modal boxes/formdata/formdata.component';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-formvalidation',
  templateUrl: './formvalidation.component.html',
  styleUrls: ['./formvalidation.component.scss']
})
export class FormvalidationComponent implements OnInit {

  constructor(
    private _formBuilder: FormBuilder,
    private http: HttpClient,
    private form: FromService,
    public dialog: MatDialog
  ) { }
  userform: FormGroup
  obj = {
    name: 'Danial',
    age: 22,
    phno: 23123123123,
    email: 'abc@gmail.com'
  }
  ngOnInit() {
    this.userform = this._formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      title: ['', Validators.required],
      body: ['', Validators.required]
    });
    this.form.getData().subscribe(res => {
      console.log(res)
    })
    // this.getData()
  }

  UserFormSubmit() {
    if (this.userform.valid) {
      console.log(this.userform.value)
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Confirm it!'
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.form.postData(this.userform.value).subscribe(res => {
            console.log(res)
            Swal.fire(
              'Success!',
              'Your file has been sent.',
              'success'
            )
          })
        }
      })
      // this.dialog.open(FormdataComponent, {
      //   width: '250px',
      //   data: this.userform.value
      // })
    }
  }
  // getData() {
  //   this.http.get('https://jsonplaceholder.typicode.com/posts').subscribe(res => {
  //     console.log(res)
  //   })
  // } 
}
