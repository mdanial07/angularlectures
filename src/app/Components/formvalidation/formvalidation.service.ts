import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FromService {
    constructor(private http: HttpClient) { }

    getData() {
        return this.http.get('https://jsonplaceholder.typicode.com/posts')
    }

    postData(post) {
        return this.http.post('https://jsonplaceholder.typicode.com/posts', post)
    }
}