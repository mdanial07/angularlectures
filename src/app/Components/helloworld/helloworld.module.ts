import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../../app-materia.module';

import { routing } from './helloworld.routing';
import { HelloworldComponent } from './helloworld.component';

@NgModule({
  declarations: [
    HelloworldComponent,
  ],
  imports: [
    routing,
    CommonModule,
    MaterialModule,
    HttpClientModule
  ]
})
export class HelloworldModule { }
