import { Routes, RouterModule } from '@angular/router';
import { HelloworldComponent } from "./helloworld.component";

const routes: Routes = [
    {
        path: '',
        component: HelloworldComponent
    }
];

export const routing = RouterModule.forChild(routes);
