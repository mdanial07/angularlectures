import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-helloworld',
  templateUrl: './helloworld.component.html',
  styleUrls: ['./helloworld.component.scss']
})
export class HelloworldComponent implements OnInit {
  name; age; sendbutton = true; editbutton = false;
  editdata;
  array = [
    { id: 1, name: 'Latesh', age: 22 },
    { id: 2, name: 'Danial', age: 22 },
    { id: 3, name: 'Muslim', age: 22 },
    { id: 4, name: 'Yassar', age: 22 },
    { id: 5, name: 'Wasseeq', age: 22 },
  ]

  apiData;
  constructor(private http: HttpClient) {
    console.log('constructor')
  }

  ngOnInit() {
    this.http.get('https://devbackend.wattcrm.com/portal/our-m2m-plan/').subscribe(res => {
      console.log(res)
      if (res['status'] == true) {
        this.apiData = res['message']
      } else {
        console.log('errrrrrrrrrr')
      }
    })

    console.log(this.array)
    console.log(this.array[this.array.length - 1]['id'])
  }
  SendData() {
    console.log(this.name)
    console.log(this.age)
    let obj = {
      id: this.array[this.array.length - 1]['id'] + 1,
      name: this.name,
      age: this.age,
    }
    this.array.push(obj)
    console.log(obj)
  }
  Delete(data) {
    console.log(data)
    this.array.map((res, i) => {
      if (res.id == data.id) {
        console.log(res)
        console.log(i)
        this.array.splice(i, 1)
      }
    })
  }
  Update(v) {
    this.sendbutton = false;
    this.editbutton = true;
    this.editdata = v
    this.name = this.editdata.name
    this.age = this.editdata.age
    // let obj = {
    //   ...this.editdata,
    //   name: this.name,
    //   age: this.age
    // }
    // console.log(obj)
  }
  UpdateFunc() {
    let obj = {
      ...this.editdata,
      name: this.name,
      age: this.age
    }

    this.array.map((res, i) => {
      if (res.id == obj.id) {
        console.log(res)
        console.log(i)
        this.array.splice(i, 1, obj)
      }
    })
    console.log(obj)
  }
}
