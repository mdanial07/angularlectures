import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from "./main.component";
import { FormvalidationComponent } from '../formvalidation/formvalidation.component';
import { TableComponent } from './table/table.component';
import { UtilitiesComponent } from './utilities/utilities.component';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        children: [
            { path: 'table', component: TableComponent },
            { path: 'utilities', component: UtilitiesComponent },
            { path: 'formvalidation', component: FormvalidationComponent },
            { path: 'hello', loadChildren: () => import('../helloworld/helloworld.module').then(res => res.HelloworldModule) }
        ]
    }
];

export const routing = RouterModule.forChild(routes);
