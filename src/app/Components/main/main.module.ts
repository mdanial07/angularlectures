import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../../app-materia.module';

import { routing } from './main.routing';
import { MainComponent } from './main.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { FormvalidationComponent } from '../formvalidation/formvalidation.component';
import { TableComponent } from './table/table.component';
import { UtilitiesComponent } from './utilities/utilities.component';
import { FromService } from '../formvalidation/formvalidation.service';
import { FormdataComponent } from '../Modal boxes/formdata/formdata.component';

@NgModule({
    declarations: [
        MainComponent,
        FooterComponent,
        HeaderComponent,
        SidenavComponent,
        FormvalidationComponent,
        TableComponent,
        UtilitiesComponent,
        FormdataComponent,
    ],
    imports: [
        routing,
        CommonModule,
        MaterialModule,
        HttpClientModule
    ],
    entryComponents: [FormdataComponent],
    providers: [FromService]
})
export class MainComponentModule { }
